=head1 NAME

bnd - Create and diagnose OSGi R4 bundles.

=head1 SYNOPSIS

B<bnd> [S<I<general-options>>] [S<I<cmd>>] [S<I<cmd-options>>]

B<bnd> [S<I<general-options>>] E<lt>fileE<gt>.jar

B<bnd> [S<I<general-options>>] E<lt>fileE<gt>.bnd

=head1 DESCRIPTION

The bnd tool helps you create and diagnose OSGi R4 bundles. The key functions are:

 * Show the manifest and JAR contents of a bundle
 * Wrap a JAR so that it becomes a bundle
 * Create a Bundle from a specification and a class path
 * Verify the validity of the manifest entries 

=head1 GENERAL OPTIONS

=over

=item -failok

Same as the property -failok. The current run will create a JAR file even if there were errors.

=item -exceptions

Will print the exception when the software has ran into a bad exception and bails out. Normally only a message is printed. For debugging or diagnostic reasons, the exception stack trace can be very helpful.

=back

=head1 COMMANDS

=over

=item print ( -verify | -manifest | -list | - all ) * E<lt>fileE<gt>.jar +

The print function will take a list of JAR file and print one or more aspect of the JAF riles. The following aspects can be added.

    * -verify - Verify the JAR for consistency with the specification
      The print will exit with an error if the verify fails.
    * -manifest - Show the manifest
    * -list - List the entries in the JAR file
    * -all - Do all (this is the default. 

C<bnd print -verify *.jar>

=item buildx ( -classpath LIST | -eclipse <lt>fileE<gt> | -noeclipse | -output E<lt>fileE<gt> ) * E<lt>fileE<gt>.bnd +

The build function will assemble a bundle from the bnd specification. The default name of the output bundle is the name of the bnd file with a .jar extension.

    * -classpath - A list of JAR files and/or directories that should
      be placed on the class path before the calculation starts.
    * -eclipse - Parse the file as an Eclipse .classpath file, use
      the information to create an Eclipse's project class path.
      If this option is used, the default .classpath file is not read
    * -noeclipse - Do not parse the .classpath file of an Eclipse
      project.
    * -output - Override the default output name of the bundle or the
      directory. If the output is a directory, the name will be
      derived from the bnd file name. 

C<bnd build -classpath bin -noeclipse -output test.jar xyz.bnd>

=item wrap ( -classpath (<lt>fileE<gt>(','<lt>fileE<gt>)*)-output <lt>fileE|dir<gt> | -properties <lt>fileE<gt> ) *
-ignoremanifest? <lt>fileE<gt>.jar *

The wrap command takes an existing JAR file and guesses the manifest headers that will make this JAR useful for an OSGi Service Platform. If the output file is not overridden, the name of the input file is used with a .bar extension. The default bnd file for the header calculation is:

 Export-Package: * 
 Import-Package: <packages inside the target jar>

If the target bundle has a manifest, the headers are merged with the properties.

The defaults can be overridden with a specific properties file.

    * -output - Set the output file or directory
    * -classpath - Sets the classpath as a comma separated list
    * -properties - Use a special property file for the manifest
      calculation.
    * -ignoremanifest - Do not include the manifest headers from
      the target bundle 

C<bnd wrap -classpath osgi.jar *.jar>

=back

=head1 SEE ALSO

L<http://www.aqute.biz/Code/Bnd>

=head1 AUTHOR

Ludovic Claude <ludovic.claude@laposte.net>

Damien Raude-Morvan <drazzib@debian.org>

