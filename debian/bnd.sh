#!/bin/sh

# Include the wrappers utility script
. /usr/lib/java-wrappers/java-wrappers.sh

# We need a java5 runtime
find_java_runtime java5

# Define our classpath
find_jars bnd

# Run bnd
run_java aQute.bnd.main.bnd $extra_args "$@"

